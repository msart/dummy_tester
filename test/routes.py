import json

from test import app
from flask import request, jsonify
import requests
import uuid

users = dict()
callback_url = None
callback_payload = {
    "result": {"primary_scores": {"social": 10, "intellect": 4}, "secondary_scores": {"social": 3, "intellect": 9}},
    "feedback_url": {"manager": "/manager_feedback", "user": "/user_feedback"}
}


@app.route('/')
@app.route('/index')
def index():
    return "Hello, World!"


@app.route('/manager_feedback')
def manager_feedback():
    return "Hello, Manager!"


@app.route('/user_feedback')
def user_feedback():
    return "Hello, User!"


@app.route('/tests/<test_code>', methods=['GET', 'DELETE'])
def tests(test_code):
    global users

    if request.method == 'GET':
        """return the information for <user_id>"""
        return jsonify(isError=False,
                       message="Success",
                       statusCode=200,
                       data={"value": 10, "code": test_code}), 200

    if request.method == 'DELETE':
        users.pop(uuid.UUID(test_code))
        return "tests {} deleted".format(test_code)


@app.route('/tests/<test_code>/finished', methods=['GET'])
def finish_callback(test_code):
    callback_payload["code"] = test_code
    requests.post(url=callback_url,
                  json=json.dumps(callback_payload))
    return "Test finished"


@app.route('/tests', methods=['POST'])
def new_tests():
    global callback_url

    number = json.loads(request.data).get('number')
    callback_url = json.loads(request.data).get('callback_url')

    created = []
    for x in range(int(number)):
        code = uuid.uuid4()
        created.append({"code": code, "url": f"/tests/{code}"})

    global users
    for created_user in created:
        users[created_user] = 10

    return jsonify(isError=False,
                   message="Success",
                   statusCode=200,
                   data=created), 200

@app.route('/feedback/<code>', methods=['GET'])
def feedback(code):
    data = {"person":code, "value":10, "html":"..."}
    return jsonify(isError= False,
                   message= "Success",
                   statusCode= 200,
                   data=data), 200


@app.route('/tests_callback', methods=['POST'])
def callback_tester():
    id = json.loads(request.data).get('id')
    result = json.loads(request.data).get('result')
    data = {"code": id, "result": result}
    print(data)
    return jsonify(isError=False,
                   message="Success",
                   statusCode=200,
                   data=data), 200

# test
#
# python run.py
#
# curl -X GET -H "Content-Type: application/json http://localhost:5000/tests/123/finished
# curl -X DELETE http://localhost:5000/tests/123
# curl -X POST -H "Content-Type: application/json" -d @test.json http://localhost:5000/tests
# curl -X GET http://localhost:5000/tests/123
# curl -X GET http://localhost:5000/feedback/123
